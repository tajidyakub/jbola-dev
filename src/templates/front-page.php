<?php get_header();?>
<main id="frontPage">
	<!-- section:kickoff -->
	<section class="container max-med">
		<div class="row">
			<div class="col py-2 mt-4">
				<h6 class="badge badge-secondary">JUST IN</h6>
			</div>
		</div>
		<div class="grid g-sm-2 g-md-4">
			<div class="card border-0">
				<img class="card-img-top"
					src="https://images.performgroup.com/di/library/GOAL/9f/9e/georginio-wijnaldum-naby-keita-sadio-mane-liverpool-2019_ib0h6ikt3m6910lul5y653ulo.jpg?t=-759451024&quality=60&w=970" alt="">
				<div class="card-body p-2">
					<h6 class="card-title my-2">
						Tetap bersama Liverpool
					</h6>
				</div>
			</div>
			<div class="card border-0">
				<img class="card-img-top"
					src="https://images.performgroup.com/di/library/GOAL/a5/df/casemiro-real-madrid_pgyh42imwm3r10ff4pgjnrmot.jpg?t=-81292072&quality=60&w=970" alt="">
				<div class="card-body p-2">
					<h6 class="card-title my-2">
						Hukuman penalti sudah tepat
					</h6>
				</div>
			</div>
			<div class="card border-0">
				<img class="card-img-top"
					src="https://images.performgroup.com/di/library/GOAL/38/49/brendan-rodgers-celtic-2018-19_140jpsht2tad512gvyw49s3hc9.jpg?t=478738313&quality=60&w=970" alt="">
				<div class="card-body p-2">
					<h6 class="card-title my-2">
						Lebih baik menunggu klub besar
					</h6>
				</div>
			</div>
			<div class="card border-0">
				<img class="card-img-top"
					src="https://images.performgroup.com/di/library/GOAL/fc/dd/sokratis-papastathopoulos-arsenal-2018-19_3ftmjjnc8ith11hdtdsdzmvdb.jpg?t=290166785&quality=60&w=970" alt="">
				<div class="card-body p-2">
					<h6 class="card-title my-2">
						Europa League Highlight
					</h6>
				</div>
			</div>
		</div>
	</section>
	<section class="container max-in">
		<div class="row">
			<div class="col py-1 mt-4">
				<h6 class="badge badge-primary">PEMBICARAAN HANGAT</h6>
			</div>
		</div>
		<div class="grid">
			<div class="card border-0">
				<img class="card-img-top"
					src="https://images.performgroup.com/di/library/omnisport/8c/aa/jurgen-klopp-cropped_b68byqpf2eqnzi7j5rjdhi30.jpg?t=128652849&quality=60&w=970"
					alt="">
				<div class="card-body px-sm-4 px-md-5 py-md-5">
					<h4 class="card-title">
						Liverpool harus sambut dengan baik setiap kesempatan yang ada.
					</h4>
					<p class="card-text mt-4">Meski hanya berhasil membukukan satu kemenangan dari empat pertandingan mereka di Premier League, Liverpool tetap masih berada di posisi puncak dan pelatih mereka masih Jurgen Klopp, salah satu yang terbaik di dunia.</p>
					<span class="text-meta text-uppercase text-muted">
						10 Feb 2019 <a href="#" class="post-tag">#LIVERPOOL</a> #EPL2018/19
					</span>
				</div>
			</div>

			<div class="card my-4">
				<ul class="list-group list-group-flush">
					<li class="list-group-item grid left-fixed l-180 ml-0 py-4">
						<div style="background-image: url(https://img.fifa.com/image/upload/t_p1/v1549994893/yhl4ecdfbmi5lkkq9udb.jpg)"
						class="img-bg bg-post-1234 minh-140"></div>
						<div class="ml-4">
							<h5>Ekuador U-20 terbaik di Amerika Selatan</h5>
							<p>Setelah pada kompetisi sebelumnya Ekuador U-20 hanya berhasil menempati posisi runner-up, tahun ini mereka berhasil menjadi yang terbaik.</p>
						</div>
					</li>
					<li class="list-group-item">Dapibus ac facilisis in</li>
					<li class="list-group-item">Vestibulum at eros</li>
				</ul>
			</div>
			<div class="card bg-brand my-3">
				<div class="grid left-fixed 120 sm-240 w-100 minh-140">
					<div style="background-image: url(https://img.fifa.com/image/upload/t_p1/v1549994893/yhl4ecdfbmi5lkkq9udb.jpg)"
						class="img-bg bg-post-1234">

					</div>
					<div class="card-body">

					</div>
				</div>
				<span class="card-footer">AMERIKA SELATAN</span>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
