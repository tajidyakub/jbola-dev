<nav class="navbar navbar-expand-sm navbar-light navbar-main py-0">
	<a class="navbar-brand" href="/">
		<?php do_action( 'wps_the_brand' ); ?>
	</a>
	<button class="navbar-toggler d-lg-none border-0" id="navbarMain" type="button" data-toggle="collapse" data-target="#collapsibleNavbarMain" aria-controls="collapsibleNavbarMain"
		aria-expanded="false" aria-label="Toggle navigation">
		<i class="fas fa-ellipsis-h fa-lg text-white"></i>
	</button>
	<div class="collapse navbar-collapse" id="collapsibleNavbarMain">
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item active">
				<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Link</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
				<div class="dropdown-menu" aria-labelledby="dropdownId">
					<a class="dropdown-item" href="#">Action 1</a>
					<a class="dropdown-item" href="#">Action 2</a>
				</div>
			</li>
		</ul>
	</div>
</nav>
