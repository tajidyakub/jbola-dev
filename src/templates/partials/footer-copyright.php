<div class="container max-in">
	<div class="row">
		<div class="col py-4 px-md-5 ">
			<p class="copyright-text text-center text-muted">
				<?php
					do_action( 'wps_the_copyright_text' ); ?>
			</p>
		</div>
	</div>
</div>
