<?php
/**
 * Theme's functions file.
 *
 * @package wpsandbox
 * @since 1.0.0
 */

if ( !function_exists( 'wps_setup' ) ) {

	function wps_setup() {
		/**
		 * Enables title tag inside <head></head>
		 */
		add_theme_support( 'title-tag' );
		/**
		 * Enables featured images
		 */
		add_theme_support( 'post-thumbnails' );
		/**
		 * Enables custom-logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 48,
				'width'       => 250,
				'flex-height' => true,
				'flex-width'  => true,
				'header-text' => array( 'site-title', 'site-description' ),
			)
		);
		/**
		 * Enables Automatic Feed Links for post and comment in the head.
		 */
		add_theme_support( 'automatic-feed-links' );
		/**
		 * Allows the use of HTML5 markup
		 */
		add_theme_support(
			'html5',
			array(
				'comment-list',
				'comment-form',
				'search-form',
				'gallery',
				'caption'
			)
		);
		/**
		 * Enables excerpt for pages.
		 */
		add_post_type_support( 'page', 'excerpt' );
	}

	/**
	 * Hook the setup function
	 */
	add_action( 'after_setup_theme', 'wps_setup' );
}

if ( !function_exists( 'wps_get_mtime' ) ) {
	function wps_get_mtime( $file ) {
		$filepath = get_template_directory() . '/' . $file;
		return filemtime( $filepath );
	}
}

if ( !function_exists( 'wps_enqueue' ) ) {
	function wps_enqueue() {
		wp_enqueue_style(
			'style',
			get_stylesheet_uri(),
			[],
			wps_get_mtime('style.css'),
			'all' );
		wp_enqueue_script(
			'main',
			get_template_directory_uri() . '/scripts/main.bundle.js',
			[],
			wps_get_mtime('/scripts/main.bundle.js'),
			true );
		wp_enqueue_script(
			'fa',
			get_template_directory_uri() . '/scripts/fa.bundle.js',
			[],
			wps_get_mtime('/scripts/fa.bundle.js'),
			true );
	}

	add_action( 'wp_enqueue_scripts', 'wps_enqueue');
}

if ( !function_exists('wps_get_feat_image') ) {
	function wps_get_feat_image( $post_id ) {
		if ( has_post_thumbnail($post_id) ) {
			$feat_img_tag = get_the_post_thumbnail( $post_id, 'full' );
			return $feat_img_tag;
		} else {
			return false;
		}
	}

	add_filter( 'get_featured_image', 'wps_get_feat_image', 1, 1 );
}

/**
 * Creates Theme's options page.
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Juarabola Theme\'s Settings',
		'menu_title'	=> 'Juarabola Settings',
		'menu_slug' 	=> 'juarabola-settings',
		'capability'	=> 'edit_posts',
		'parent_slug' 	=> 'options-general.php',
		'redirect'		=> false,
		'icon_url'		=> 'dashicons-editor-code'
	));
}

/**
 * Extract Brand Text from the options.
 */

function wps_get_option( $field ) {
	$value	= get_field( $field, 'options');
	return $value;
}

add_filter( 'get_option', 'wps_get_option', 1, 1);

function wps_the_copyright_text_cb() {
	echo  do_shortcode( apply_filters( 'get_option', 'footer_copyright_text' ) );
}

add_action( 'wps_the_copyright_text', 'wps_the_copyright_text_cb' );

function wps_brand_cb() {
	$web_title = apply_filters('get_option', 'website_title');
	if ( has_custom_logo() ) {
		$custom_logo_id = get_theme_mod( 'custom_logo' );
		$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
		echo "<img src=\"{$image[0]}\" alt=\"{$web_title}\" height=\"32\"/>";
	} else {
		echo $web_title;
	}
}

add_action( 'wps_the_brand', 'wps_brand_cb' );

function wps_badge_shortcode_cb( $atts = [] ) {
	$web_title = apply_filters('get_option', 'website_title');
	return "<a href=\"/\" class=\"badge badge-brand text-uppercase text-regular p-1 mx-1\">{$web_title}</a>";
}

add_shortcode( 'badge', 'wps_badge_shortcode_cb' );
