import { library, dom } from '@fortawesome/fontawesome-svg-core';
import {
	faCopyright,
	faHome,
	faRegistered,
	faSitemap,
	faEllipsisH
} from '@fortawesome/free-solid-svg-icons';
import { faComments } from '@fortawesome/free-regular-svg-icons/faComments';
import { faUserCircle } from '@fortawesome/free-regular-svg-icons/faUserCircle';
import { faPlusSquare } from '@fortawesome/free-regular-svg-icons/faPlusSquare';

library.add(faCopyright);
library.add(faEllipsisH);
library.add(faSitemap);
library.add(faRegistered);
library.add(faHome);
library.add(faComments);
library.add(faUserCircle);
library.add(faPlusSquare);

dom.watch();
